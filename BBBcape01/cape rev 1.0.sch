EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:pesd12vl2bt
LIBS:DS3231SN
LIBS:cape rev 1.0-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "24 jul 2014"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_10X2 P9
U 1 1 5387678D
P 1450 5250
F 0 "P9" H 1450 5800 60  0000 C CNN
F 1 "HDR 2X10" V 1450 5150 50  0000 C CNN
F 2 "" H 1450 5250 60  0000 C CNN
F 3 "" H 1450 5250 60  0000 C CNN
	1    1450 5250
	-1   0    0    1   
$EndComp
$Comp
L CONN_10X2 P8
U 1 1 5387679C
P 1400 6750
F 0 "P8" H 1400 7300 60  0000 C CNN
F 1 "BAL HDR 2X10" V 1400 6650 50  0000 C CNN
F 2 "" H 1400 6750 60  0000 C CNN
F 3 "" H 1400 6750 60  0000 C CNN
	1    1400 6750
	-1   0    0    1   
$EndComp
$Comp
L CONN_4 P3
U 1 1 53876BA5
P 3150 7250
F 0 "P3" V 3100 7250 50  0000 C CNN
F 1 "BAL HDR 2X2" V 3200 7250 50  0000 C CNN
F 2 "" H 3150 7250 60  0000 C CNN
F 3 "" H 3150 7250 60  0000 C CNN
	1    3150 7250
	1    0    0    -1  
$EndComp
$Comp
L CONN_4 P4
U 1 1 53876BB4
P 2650 7250
F 0 "P4" V 2600 7250 50  0000 C CNN
F 1 "BAL HDR 2X2" V 2700 7250 50  0000 C CNN
F 2 "" H 2650 7250 60  0000 C CNN
F 3 "" H 2650 7250 60  0000 C CNN
	1    2650 7250
	1    0    0    -1  
$EndComp
$Comp
L CONN_5X2 P2
U 1 1 53876C76
P 4150 3950
F 0 "P2" H 4150 4250 60  0000 C CNN
F 1 "SER HDR 2X5" V 4150 3950 50  0000 C CNN
F 2 "" H 4150 3950 60  0000 C CNN
F 3 "" H 4150 3950 60  0000 C CNN
	1    4150 3950
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 53876D9C
P 3650 5500
F 0 "R1" V 3730 5500 40  0001 C CNN
F 1 "R1" V 3657 5501 40  0000 C CNN
F 2 "~" V 3580 5500 30  0000 C CNN
F 3 "~" H 3650 5500 30  0000 C CNN
	1    3650 5500
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 53876DAB
P 3850 5500
F 0 "R2" V 3930 5500 40  0001 C CNN
F 1 "R2" V 3857 5501 40  0000 C CNN
F 2 "~" V 3780 5500 30  0000 C CNN
F 3 "~" H 3850 5500 30  0000 C CNN
	1    3850 5500
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 53876DBA
P 800 2150
F 0 "R3" V 880 2150 40  0001 C CNN
F 1 "R3" V 807 2151 40  0000 C CNN
F 2 "~" V 730 2150 30  0000 C CNN
F 3 "~" H 800 2150 30  0000 C CNN
	1    800  2150
	-1   0    0    1   
$EndComp
$Comp
L R R4
U 1 1 53876DC9
P 650 2150
F 0 "R4" V 730 2150 40  0001 C CNN
F 1 "R4" V 657 2151 40  0000 C CNN
F 2 "~" V 580 2150 30  0000 C CNN
F 3 "~" H 650 2150 30  0000 C CNN
	1    650  2150
	-1   0    0    1   
$EndComp
$Comp
L R R5
U 1 1 53876DD8
P 2950 2150
F 0 "R5" V 3030 2150 40  0001 C CNN
F 1 "R5" V 2957 2151 40  0000 C CNN
F 2 "~" V 2880 2150 30  0000 C CNN
F 3 "~" H 2950 2150 30  0000 C CNN
	1    2950 2150
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 53876DE7
P 3100 2150
F 0 "R6" V 3180 2150 40  0001 C CNN
F 1 "R6" V 3107 2151 40  0000 C CNN
F 2 "~" V 3030 2150 30  0000 C CNN
F 3 "~" H 3100 2150 30  0000 C CNN
	1    3100 2150
	-1   0    0    1   
$EndComp
$Comp
L R R7
U 1 1 53876DF6
P 1000 1050
F 0 "R7" V 1080 1050 40  0001 C CNN
F 1 "R7" V 1007 1051 40  0000 C CNN
F 2 "~" V 930 1050 30  0000 C CNN
F 3 "~" H 1000 1050 30  0000 C CNN
	1    1000 1050
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 53876E05
P 2750 1050
F 0 "R8" V 2830 1050 40  0001 C CNN
F 1 "R8" V 2757 1051 40  0000 C CNN
F 2 "~" V 2680 1050 30  0000 C CNN
F 3 "~" H 2750 1050 30  0000 C CNN
	1    2750 1050
	-1   0    0    1   
$EndComp
$Comp
L CONN_6 J1
U 1 1 53876EB8
P 3050 5150
F 0 "J1" V 3000 5150 60  0000 C CNN
F 1 "HDR 1X6" V 3100 5150 60  0000 C CNN
F 2 "" H 3050 5150 60  0000 C CNN
F 3 "" H 3050 5150 60  0000 C CNN
	1    3050 5150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 53876F47
P 5400 3750
F 0 "#PWR01" H 5400 3750 30  0001 C CNN
F 1 "GND" H 5400 3680 30  0001 C CNN
F 2 "" H 5400 3750 60  0000 C CNN
F 3 "" H 5400 3750 60  0000 C CNN
	1    5400 3750
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR02
U 1 1 53876F56
P 5350 2500
F 0 "#PWR02" H 5350 2500 30  0001 C CNN
F 1 "GND" H 5350 2430 30  0001 C CNN
F 2 "" H 5350 2500 60  0000 C CNN
F 3 "" H 5350 2500 60  0000 C CNN
	1    5350 2500
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR03
U 1 1 53876F65
P 6700 1700
F 0 "#PWR03" H 6700 1700 30  0001 C CNN
F 1 "GND" H 6700 1630 30  0001 C CNN
F 2 "" H 6700 1700 60  0000 C CNN
F 3 "" H 6700 1700 60  0000 C CNN
	1    6700 1700
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR04
U 1 1 53876F74
P 5900 1450
F 0 "#PWR04" H 5900 1450 30  0001 C CNN
F 1 "GND" H 5900 1380 30  0001 C CNN
F 2 "" H 5900 1450 60  0000 C CNN
F 3 "" H 5900 1450 60  0000 C CNN
	1    5900 1450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 53876F83
P 1450 5950
F 0 "#PWR05" H 1450 5950 30  0001 C CNN
F 1 "GND" H 1450 5880 30  0001 C CNN
F 2 "" H 1450 5950 60  0000 C CNN
F 3 "" H 1450 5950 60  0000 C CNN
	1    1450 5950
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR06
U 1 1 53876F92
P 2150 5600
F 0 "#PWR06" H 2150 5700 30  0001 C CNN
F 1 "VDD" H 2150 5710 30  0000 C CNN
F 2 "" H 2150 5600 60  0000 C CNN
F 3 "" H 2150 5600 60  0000 C CNN
	1    2150 5600
	0    1    1    0   
$EndComp
$Comp
L VDD #PWR07
U 1 1 53876FA1
P 650 5700
F 0 "#PWR07" H 650 5800 30  0001 C CNN
F 1 "VDD" H 650 5810 30  0000 C CNN
F 2 "" H 650 5700 60  0000 C CNN
F 3 "" H 650 5700 60  0000 C CNN
	1    650  5700
	0    -1   -1   0   
$EndComp
$Comp
L VDD #PWR08
U 1 1 53876FB0
P 2500 6200
F 0 "#PWR08" H 2500 6300 30  0001 C CNN
F 1 "VDD" H 2500 6310 30  0000 C CNN
F 2 "" H 2500 6200 60  0000 C CNN
F 3 "" H 2500 6200 60  0000 C CNN
	1    2500 6200
	-1   0    0    1   
$EndComp
$Comp
L C C1
U 1 1 538775D1
P 3500 800
F 0 "C1" H 3500 900 40  0000 L CNN
F 1 "C1" H 3506 715 40  0001 L CNN
F 2 "~" H 3538 650 30  0000 C CNN
F 3 "~" H 3500 800 60  0000 C CNN
	1    3500 800 
	0    -1   -1   0   
$EndComp
$Comp
L C C3
U 1 1 538775EF
P 6150 1200
F 0 "C3" H 6150 1300 40  0000 R CNN
F 1 "C3" H 6156 1115 40  0001 R CNN
F 2 "~" H 6188 1050 30  0000 C CNN
F 3 "~" H 6150 1200 60  0000 C CNN
	1    6150 1200
	0    -1   1    0   
$EndComp
$Comp
L C C4
U 1 1 538775FE
P 5650 800
F 0 "C4" H 5650 900 40  0000 L CNN
F 1 "C4" H 5656 715 40  0001 L CNN
F 2 "~" H 5688 650 30  0000 C CNN
F 3 "~" H 5650 800 60  0000 C CNN
	1    5650 800 
	0    -1   -1   0   
$EndComp
$Comp
L C C5
U 1 1 5387760D
P 6150 1700
F 0 "C5" H 6150 1800 40  0000 L CNN
F 1 "C5" H 6156 1615 40  0001 L CNN
F 2 "~" H 6188 1550 30  0000 C CNN
F 3 "~" H 6150 1700 60  0000 C CNN
	1    6150 1700
	0    -1   -1   0   
$EndComp
$Comp
L C C2
U 1 1 538775E0
P 3500 1300
F 0 "C2" H 3500 1400 40  0000 L CNN
F 1 "C2" H 3506 1215 40  0001 L CNN
F 2 "~" H 3538 1150 30  0000 C CNN
F 3 "~" H 3500 1300 60  0000 C CNN
	1    3500 1300
	0    -1   -1   0   
$EndComp
$Comp
L MAX232 U1
U 1 1 538765A5
P 4550 1500
F 0 "U1" H 4550 2350 70  0000 C CNN
F 1 "MAX3232" H 4550 650 70  0000 C CNN
F 2 "" H 4550 1500 60  0000 C CNN
F 3 "" H 4550 1500 60  0000 C CNN
	1    4550 1500
	1    0    0    -1  
$EndComp
$Comp
L RTC_CLK U2
U 1 1 538786D8
P 5000 6100
F 0 "U2" H 5000 6100 60  0000 C BNN
F 1 "RTC CLK" H 5000 6100 60  0000 C TNN
F 2 "" H 5000 6100 60  0000 C CNN
F 3 "" H 5000 6100 60  0000 C CNN
	1    5000 6100
	1    0    0    -1  
$EndComp
$Comp
L C C6
U 1 1 538786F6
P 6050 6750
F 0 "C6" H 6050 6850 40  0000 L CNN
F 1 "C6" H 6056 6665 40  0001 L CNN
F 2 "~" H 6088 6600 30  0000 C CNN
F 3 "~" H 6050 6750 60  0000 C CNN
	1    6050 6750
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 53878705
P 6950 1750
F 0 "R9" V 7030 1750 40  0001 C CNN
F 1 "R9" V 6957 1751 40  0000 C CNN
F 2 "~" V 6880 1750 30  0000 C CNN
F 3 "~" H 6950 1750 30  0000 C CNN
	1    6950 1750
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 53878714
P 9250 1750
F 0 "R10" V 9330 1750 40  0001 C CNN
F 1 "R10" V 9257 1751 40  0000 C CNN
F 2 "~" V 9180 1750 30  0000 C CNN
F 3 "~" H 9250 1750 30  0000 C CNN
	1    9250 1750
	-1   0    0    1   
$EndComp
$Comp
L C C7
U 1 1 53878723
P 5450 5200
F 0 "C7" H 5450 5300 40  0000 L CNN
F 1 "C7" H 5456 5115 40  0000 L CNN
F 2 "~" H 5488 5050 30  0000 C CNN
F 3 "~" H 5450 5200 60  0000 C CNN
	1    5450 5200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR09
U 1 1 53878732
P 5000 7100
F 0 "#PWR09" H 5000 7100 30  0001 C CNN
F 1 "GND" H 5000 7030 30  0001 C CNN
F 2 "" H 5000 7100 60  0000 C CNN
F 3 "" H 5000 7100 60  0000 C CNN
	1    5000 7100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 53878741
P 6050 7150
F 0 "#PWR010" H 6050 7150 30  0001 C CNN
F 1 "GND" H 6050 7080 30  0001 C CNN
F 2 "" H 6050 7150 60  0000 C CNN
F 3 "" H 6050 7150 60  0000 C CNN
	1    6050 7150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 53878750
P 6350 7150
F 0 "#PWR011" H 6350 7150 30  0001 C CNN
F 1 "GND" H 6350 7080 30  0001 C CNN
F 2 "" H 6350 7150 60  0000 C CNN
F 3 "" H 6350 7150 60  0000 C CNN
	1    6350 7150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5387875F
P 5800 5200
F 0 "#PWR012" H 5800 5200 30  0001 C CNN
F 1 "GND" H 5800 5130 30  0001 C CNN
F 2 "" H 5800 5200 60  0000 C CNN
F 3 "" H 5800 5200 60  0000 C CNN
	1    5800 5200
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR013
U 1 1 5387876E
P 6700 7150
F 0 "#PWR013" H 6700 7150 30  0001 C CNN
F 1 "GND" H 6700 7080 30  0001 C CNN
F 2 "" H 6700 7150 60  0000 C CNN
F 3 "" H 6700 7150 60  0000 C CNN
	1    6700 7150
	1    0    0    -1  
$EndComp
$Comp
L BATTERY RTC1
U 1 1 53878BAE
P 6700 6750
F 0 "RTC1" V 6700 6950 50  0000 C CNN
F 1 "3V" V 6700 6560 50  0000 C CNN
F 2 "~" H 6700 6750 60  0000 C CNN
F 3 "~" H 6700 6750 60  0000 C CNN
	1    6700 6750
	0    1    1    0   
$EndComp
$Comp
L CONN_2 P5
U 1 1 5387993A
P 2400 6750
F 0 "P5" V 2350 6750 40  0000 C CNN
F 1 "GND JUMP" V 2450 6750 40  0000 C CNN
F 2 "" H 2400 6750 60  0000 C CNN
F 3 "" H 2400 6750 60  0000 C CNN
	1    2400 6750
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR014
U 1 1 53879DD2
P 3050 6650
F 0 "#PWR014" H 3050 6650 30  0001 C CNN
F 1 "GND" H 3050 6580 30  0001 C CNN
F 2 "" H 3050 6650 60  0000 C CNN
F 3 "" H 3050 6650 60  0000 C CNN
	1    3050 6650
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR015
U 1 1 53879DE1
P 2750 6250
F 0 "#PWR015" H 2750 6250 30  0001 C CNN
F 1 "GND" H 2750 6180 30  0001 C CNN
F 2 "" H 2750 6250 60  0000 C CNN
F 3 "" H 2750 6250 60  0000 C CNN
	1    2750 6250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG016
U 1 1 53879F38
P 2500 5900
F 0 "#FLG016" H 2500 5995 30  0001 C CNN
F 1 "PWR_FLAG" H 2500 6080 30  0000 C CNN
F 2 "" H 2500 5900 60  0000 C CNN
F 3 "" H 2500 5900 60  0000 C CNN
	1    2500 5900
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG017
U 1 1 53879F56
P 2750 5900
F 0 "#FLG017" H 2750 5995 30  0001 C CNN
F 1 "PWR_FLAG" H 2750 6080 30  0000 C CNN
F 2 "" H 2750 5900 60  0000 C CNN
F 3 "" H 2750 5900 60  0000 C CNN
	1    2750 5900
	1    0    0    -1  
$EndComp
$Comp
L DIODE D1
U 1 1 53A06791
P 6350 6750
F 0 "D1" V 6350 6850 40  0000 C CNN
F 1 "D1" H 6350 6650 40  0001 C CNN
F 2 "~" H 6350 6750 60  0000 C CNN
F 3 "~" H 6350 6750 60  0000 C CNN
	1    6350 6750
	0    1    1    0   
$EndComp
$Comp
L FDC6330L U3
U 1 1 53AC53A3
P 1900 1600
F 0 "U3" H 1900 1200 60  0000 C CNN
F 1 "FDC6330L" H 1950 1350 60  0000 C CNN
F 2 "" H 1950 1200 60  0000 C CNN
F 3 "" H 1950 1200 60  0000 C CNN
	1    1900 1600
	0    1    1    0   
$EndComp
$Comp
L SMS24C U5
U 1 1 53AC5E0E
P 1550 3800
F 0 "U5" H 1550 3400 60  0000 C CNN
F 1 "SMS24C" H 1600 3550 60  0000 C CNN
F 2 "" H 1600 3400 60  0000 C CNN
F 3 "" H 1600 3400 60  0000 C CNN
	1    1550 3800
	0    -1   -1   0   
$EndComp
$Comp
L C C8
U 1 1 53AC5E50
P 1450 2500
F 0 "C8" H 1450 2600 40  0000 L CNN
F 1 "C8" H 1456 2415 40  0001 L CNN
F 2 "~" H 1488 2350 30  0000 C CNN
F 3 "~" H 1450 2500 60  0000 C CNN
	1    1450 2500
	0    1    1    0   
$EndComp
$Comp
L C C9
U 1 1 53AC5E5F
P 1450 3050
F 0 "C9" H 1450 3150 40  0000 L CNN
F 1 "C9" H 1456 2965 40  0001 L CNN
F 2 "~" H 1488 2900 30  0000 C CNN
F 3 "~" H 1450 3050 60  0000 C CNN
	1    1450 3050
	0    1    1    0   
$EndComp
$Comp
L C C10
U 1 1 53AC5E6E
P 1100 4250
F 0 "C10" H 1100 4350 40  0000 L CNN
F 1 "C10" H 1106 4165 40  0001 L CNN
F 2 "~" H 1138 4100 30  0000 C CNN
F 3 "~" H 1100 4250 60  0000 C CNN
	1    1100 4250
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 53AC5E7D
P 6500 2300
F 0 "R11" V 6580 2300 40  0001 C CNN
F 1 "R11" V 6507 2301 40  0000 C CNN
F 2 "~" V 6430 2300 30  0000 C CNN
F 3 "~" H 6500 2300 30  0000 C CNN
	1    6500 2300
	0    -1   -1   0   
$EndComp
$Comp
L R R12
U 1 1 53AC5E8C
P 6500 2600
F 0 "R12" V 6580 2600 40  0001 C CNN
F 1 "R12" V 6507 2601 40  0000 C CNN
F 2 "~" V 6430 2600 30  0000 C CNN
F 3 "~" H 6500 2600 30  0000 C CNN
	1    6500 2600
	0    -1   -1   0   
$EndComp
$Comp
L R R13
U 1 1 53AC5E9B
P 6500 4000
F 0 "R13" V 6580 4000 40  0001 C CNN
F 1 "R13" V 6507 4001 40  0000 C CNN
F 2 "~" V 6430 4000 30  0000 C CNN
F 3 "~" H 6500 4000 30  0000 C CNN
	1    6500 4000
	0    -1   -1   0   
$EndComp
$Comp
L R R14
U 1 1 53AC5EAA
P 6500 4300
F 0 "R14" V 6580 4300 40  0001 C CNN
F 1 "R14" V 6507 4301 40  0000 C CNN
F 2 "~" V 6430 4300 30  0000 C CNN
F 3 "~" H 6500 4300 30  0000 C CNN
	1    6500 4300
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR018
U 1 1 53AC77B8
P 1850 4550
F 0 "#PWR018" H 1850 4550 30  0001 C CNN
F 1 "GND" H 1850 4480 30  0001 C CNN
F 2 "" H 1850 4550 60  0000 C CNN
F 3 "" H 1850 4550 60  0000 C CNN
	1    1850 4550
	1    0    0    -1  
$EndComp
$Comp
L FDC6330L U4
U 1 1 53B2ED97
P 1850 1600
F 0 "U4" H 1850 1200 60  0000 C CNN
F 1 "FDC6330L" H 1900 1350 60  0000 C CNN
F 2 "~" H 1900 1200 60  0000 C CNN
F 3 "~" H 1900 1200 60  0000 C CNN
	1    1850 1600
	0    -1   1    0   
$EndComp
$Comp
L C C11
U 1 1 53B2EE2D
P 2300 2500
F 0 "C11" H 2300 2600 40  0000 L CNN
F 1 "C11" H 2306 2415 40  0001 L CNN
F 2 "~" H 2338 2350 30  0000 C CNN
F 3 "~" H 2300 2500 60  0000 C CNN
	1    2300 2500
	0    -1   -1   0   
$EndComp
$Comp
L C C12
U 1 1 53B2EE6A
P 2300 3050
F 0 "C12" H 2300 3150 40  0000 L CNN
F 1 "C12" H 2306 2965 40  0001 L CNN
F 2 "~" H 2338 2900 30  0000 C CNN
F 3 "~" H 2300 3050 60  0000 C CNN
	1    2300 3050
	0    -1   -1   0   
$EndComp
$Comp
L C C13
U 1 1 53B2EE79
P 2650 4250
F 0 "C13" H 2650 4350 40  0000 L CNN
F 1 "C13" H 2656 4165 40  0001 L CNN
F 2 "~" H 2688 4100 30  0000 C CNN
F 3 "~" H 2650 4250 60  0000 C CNN
	1    2650 4250
	1    0    0    -1  
$EndComp
Text GLabel 1900 6500 2    39   Output ~ 0
P8_15
Text GLabel 650  1500 1    39   Input ~ 0
P8_15
Text GLabel 3100 1500 1    39   Input ~ 0
P8_14
Text GLabel 3550 6100 0    39   BiDi ~ 0
P9_18
Text GLabel 950  4900 0    39   BiDi ~ 0
P9_18
Text GLabel 1950 4900 2    39   Output ~ 0
P9_17
Text GLabel 3550 5800 0    39   Input ~ 0
P9_17
Text GLabel 900  6600 0    39   Output ~ 0
P8_14
Text GLabel 850  5600 0    39   UnSpc ~ 0
P9_4_3V3
Text GLabel 3500 5200 0    39   UnSpc ~ 0
P9_4_3V3
Text GLabel 3650 2100 0    39   Output ~ 0
A0Rx
Text GLabel 1950 5200 2    39   Input ~ 0
A4Rx
Text GLabel 2600 5300 0    39   Output ~ 0
A0Tx
Text GLabel 2600 5200 0    39   Input ~ 0
A0Rx
Text GLabel 3650 2200 0    39   Output ~ 0
A4Rx
Text GLabel 3650 1900 0    39   Input ~ 0
A0Tx
Text GLabel 3650 2000 0    39   Input ~ 0
A4Tx
Text GLabel 1950 5100 2    39   Output ~ 0
A4Tx
Text GLabel 4600 3500 0    39   Input ~ 0
B0Tx
Text GLabel 5450 1900 2    39   Output ~ 0
B0Tx
Text GLabel 5450 2000 2    39   Output ~ 0
B4Tx
Text GLabel 4650 4750 0    39   Input ~ 0
B4Tx
Text GLabel 4600 3400 0    39   Output ~ 0
B0Rx
Text GLabel 5450 2100 2    39   Input ~ 0
B0Rx
Text GLabel 4650 4650 0    39   Output ~ 0
B4Rx
Text GLabel 5450 2200 2    39   Input ~ 0
B4Rx
Text GLabel 2600 4900 0    39   BiDi ~ 0
GND JMP
Text GLabel 2800 6850 2    39   BiDi ~ 0
GND JMP
$Comp
L GND #PWR019
U 1 1 53B33A3B
P 8100 2100
F 0 "#PWR019" H 8100 2100 30  0001 C CNN
F 1 "GND" H 8100 2030 30  0001 C CNN
F 2 "" H 8100 2100 60  0000 C CNN
F 3 "" H 8100 2100 60  0000 C CNN
	1    8100 2100
	1    0    0    -1  
$EndComp
Text GLabel 8950 1050 2    39   Output ~ 0
U5_12_OUT
Text GLabel 1900 6700 2    39   Input ~ 0
P8_11
Text GLabel 6950 1400 1    39   Input ~ 0
P8_11
Text GLabel 9250 1400 1    39   Input ~ 0
P8_12
Text GLabel 900  6700 0    39   Input ~ 0
P8_12
$Comp
L SMS15C U6
U 1 1 53AC5D54
P 7800 1250
F 0 "U6" H 7800 850 60  0000 C CNN
F 1 "SMS15C" H 7850 1000 60  0000 C CNN
F 2 "" H 7850 850 60  0000 C CNN
F 3 "" H 7850 850 60  0000 C CNN
	1    7800 1250
	0    -1   -1   0   
$EndComp
Text GLabel 6150 4000 0    39   BiDi ~ 0
P9_14
Text GLabel 950  5100 0    39   Input ~ 0
P9_14
Text GLabel 6150 4300 0    39   BiDi ~ 0
P9_16
Text GLabel 950  5000 0    39   Input ~ 0
P9_16
$Comp
L GND #PWR020
U 1 1 53B34C2B
P 7750 3300
F 0 "#PWR020" H 7750 3300 30  0001 C CNN
F 1 "GND" H 7750 3230 30  0001 C CNN
F 2 "" H 7750 3300 60  0000 C CNN
F 3 "" H 7750 3300 60  0000 C CNN
	1    7750 3300
	0    -1   -1   0   
$EndComp
Text GLabel 6150 2600 0    39   BiDi ~ 0
P8_19
Text GLabel 6150 2300 0    39   BiDi ~ 0
P8_13
Text GLabel 7250 1050 0    39   Output ~ 0
U5_11_OUT
$Comp
L SMS05C U7
U 1 1 53AC5DFF
P 7000 3000
F 0 "U7" H 7000 2600 60  0000 C CNN
F 1 "SMS05C" H 7050 2750 60  0000 C CNN
F 2 "" H 7050 2600 60  0000 C CNN
F 3 "" H 7050 2600 60  0000 C CNN
	1    7000 3000
	-1   0    0    -1  
$EndComp
Text GLabel 5350 600  0    39   UnSpc ~ 0
P9_4
Text GLabel 7550 2300 2    39   Output ~ 0
U7_13_OUT
Text GLabel 7550 2600 2    39   Output ~ 0
U7_19_OUT
Text GLabel 7550 4000 2    39   Output ~ 0
U7_14_OUT
Text GLabel 7550 4300 2    39   Output ~ 0
U7_16_OUT
Text GLabel 1650 2800 0    39   Output ~ 0
U3_OUT
Text GLabel 2100 2800 2    39   Output ~ 0
U4_OUT
Text GLabel 9500 6350 0    39   Input ~ 0
U5_12_OUT
Text GLabel 9500 6250 0    39   Input ~ 0
U5_11_OUT
Text GLabel 9500 6450 0    39   Input ~ 0
U7_13_OUT
Text GLabel 9500 6550 0    39   Input ~ 0
U7_19_OUT
Text GLabel 9500 6650 0    39   Input ~ 0
U7_14_OUT
Text GLabel 9500 6750 0    39   Input ~ 0
U7_16_OUT
Text GLabel 10500 6550 2    39   Input ~ 0
U4_OUT
Text GLabel 10500 6650 2    39   Input ~ 0
U3_OUT
Text GLabel 1750 650  0    39   UnSpc ~ 0
+15V_IN
Text GLabel 10500 6350 2    39   UnSpc ~ 0
+15V_IN
Text GLabel 9500 6150 0    39   UnSpc ~ 0
P9_4_3V3
Text GLabel 10500 6850 2    39   UnSpc ~ 0
P9_4_3V3
$Comp
L CONN_8X2 P6
U 1 1 53BED187
P 10000 6500
F 0 "P6" H 10000 6950 60  0000 C CNN
F 1 "CONN_8X2" V 10000 6500 50  0000 C CNN
F 2 "" H 10000 6500 60  0000 C CNN
F 3 "" H 10000 6500 60  0000 C CNN
	1    10000 6500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 53BED196
P 9100 6850
F 0 "#PWR021" H 9100 6850 30  0001 C CNN
F 1 "GND" H 9100 6780 30  0001 C CNN
F 2 "" H 9100 6850 60  0000 C CNN
F 3 "" H 9100 6850 60  0000 C CNN
	1    9100 6850
	0    1    1    0   
$EndComp
$Comp
L GND #PWR022
U 1 1 53BED1A5
P 10800 6150
F 0 "#PWR022" H 10800 6150 30  0001 C CNN
F 1 "GND" H 10800 6080 30  0001 C CNN
F 2 "" H 10800 6150 60  0000 C CNN
F 3 "" H 10800 6150 60  0000 C CNN
	1    10800 6150
	0    -1   -1   0   
$EndComp
$Comp
L CONN_1 P10
U 1 1 53BEE776
P 10850 6250
F 0 "P10" H 10930 6250 40  0000 L CNN
F 1 "CONN_1" H 10850 6305 30  0001 C CNN
F 2 "" H 10850 6250 60  0000 C CNN
F 3 "" H 10850 6250 60  0000 C CNN
	1    10850 6250
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P11
U 1 1 53BEE785
P 10850 6450
F 0 "P11" H 10930 6450 40  0000 L CNN
F 1 "CONN_1" H 10850 6505 30  0001 C CNN
F 2 "" H 10850 6450 60  0000 C CNN
F 3 "" H 10850 6450 60  0000 C CNN
	1    10850 6450
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 P12
U 1 1 53BEE794
P 10850 6750
F 0 "P12" H 10930 6750 40  0000 L CNN
F 1 "CONN_1" H 10850 6805 30  0001 C CNN
F 2 "" H 10850 6750 60  0000 C CNN
F 3 "" H 10850 6750 60  0000 C CNN
	1    10850 6750
	1    0    0    -1  
$EndComp
$Comp
L 2N7002 U9
U 1 1 53BECB6D
P 8900 1450
F 0 "U9" H 8900 1500 60  0000 C CNN
F 1 "2N7002" H 8900 1400 60  0000 C CNN
F 2 "" H 8900 1500 60  0000 C CNN
F 3 "" H 8900 1500 60  0000 C CNN
	1    8900 1450
	0    1    -1   0   
$EndComp
$Comp
L 2N7002 U8
U 1 1 53BECB8A
P 7300 1450
F 0 "U8" H 7300 1500 60  0000 C CNN
F 1 "2N7002" H 7300 1400 60  0000 C CNN
F 2 "" H 7300 1500 60  0000 C CNN
F 3 "" H 7300 1500 60  0000 C CNN
	1    7300 1450
	0    -1   -1   0   
$EndComp
$Comp
L CONN_5X2 P1
U 1 1 53876C67
P 4100 2700
F 0 "P1" H 4100 3000 60  0000 C CNN
F 1 "SER HDR 2X5" V 4100 2700 50  0000 C CNN
F 2 "" H 4100 2700 60  0000 C CNN
F 3 "" H 4100 2700 60  0000 C CNN
	1    4100 2700
	-1   0    0    1   
$EndComp
$Comp
L 12V_ESD D3
U 1 1 53C7F669
P 5150 4250
F 0 "D3" H 5150 4200 60  0000 C CNN
F 1 "12V_ESD" H 5150 4300 60  0000 C CNN
F 2 "" H 5150 4200 60  0000 C CNN
F 3 "" H 5150 4200 60  0000 C CNN
	1    5150 4250
	1    0    0    -1  
$EndComp
$Comp
L 12V_ESD D2
U 1 1 53C7F678
P 5100 3000
F 0 "D2" H 5100 2950 60  0000 C CNN
F 1 "12V_ESD" H 5100 3050 60  0000 C CNN
F 2 "" H 5100 2950 60  0000 C CNN
F 3 "" H 5100 2950 60  0000 C CNN
	1    5100 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1200 5950 1200
Wire Wire Line
	6350 1200 6400 1200
Wire Wire Line
	6400 1200 6400 1700
Wire Wire Line
	6350 1700 6700 1700
Wire Wire Line
	5950 1700 5350 1700
Connection ~ 6400 1700
Connection ~ 5900 1400
Wire Wire Line
	3750 1200 3700 1200
Wire Wire Line
	3700 1200 3700 1050
Wire Wire Line
	3700 1050 3250 1050
Wire Wire Line
	3250 1050 3250 800 
Wire Wire Line
	3250 800  3300 800 
Wire Wire Line
	3700 800  3750 800 
Wire Wire Line
	3750 1300 3700 1300
Wire Wire Line
	3300 1300 3250 1300
Wire Wire Line
	3250 1300 3250 1550
Wire Wire Line
	3250 1550 3700 1550
Wire Wire Line
	3700 1550 3700 1700
Wire Wire Line
	3700 1700 3750 1700
Wire Wire Line
	4600 6900 4600 7000
Wire Wire Line
	4600 7000 5400 7000
Wire Wire Line
	5400 7000 5400 6900
Wire Wire Line
	4700 6900 4700 7000
Connection ~ 4700 7000
Wire Wire Line
	4800 6900 4800 7000
Connection ~ 4800 7000
Wire Wire Line
	4900 6900 4900 7000
Connection ~ 4900 7000
Wire Wire Line
	5000 6900 5000 7100
Connection ~ 5000 7000
Wire Wire Line
	5100 6900 5100 7000
Connection ~ 5100 7000
Wire Wire Line
	5200 6900 5200 7000
Connection ~ 5200 7000
Wire Wire Line
	5300 6900 5300 7000
Connection ~ 5300 7000
Wire Wire Line
	6700 6400 6700 6450
Wire Wire Line
	5800 6400 6700 6400
Wire Wire Line
	6350 6550 6350 6400
Connection ~ 6350 6400
Wire Wire Line
	6050 6550 6050 6400
Connection ~ 6050 6400
Wire Wire Line
	6700 7050 6700 7150
Wire Wire Line
	6350 6950 6350 7150
Wire Wire Line
	6050 6950 6050 7150
Wire Wire Line
	5650 5200 5800 5200
Wire Wire Line
	5000 5200 5000 5300
Wire Wire Line
	3650 5200 3650 5250
Connection ~ 5000 5200
Wire Wire Line
	3850 5200 3850 5250
Connection ~ 3850 5200
Wire Wire Line
	3650 5750 3650 5800
Wire Wire Line
	3550 5800 4200 5800
Wire Wire Line
	3850 5750 3850 6100
Connection ~ 3650 5800
Connection ~ 3850 6100
Connection ~ 3650 5200
Wire Wire Line
	1050 5700 1000 5700
Wire Wire Line
	1000 5700 1000 5900
Wire Wire Line
	1000 5900 1900 5900
Wire Wire Line
	1900 5900 1900 5700
Wire Wire Line
	1900 5700 1850 5700
Wire Wire Line
	1450 5950 1450 5900
Connection ~ 1450 5900
Wire Wire Line
	2150 5600 1850 5600
Wire Wire Line
	5900 800  5900 1450
Wire Wire Line
	5900 1400 5350 1400
Wire Wire Line
	5900 800  5850 800 
Wire Wire Line
	5350 800  5450 800 
Wire Wire Line
	2750 6650 3050 6650
Wire Wire Line
	2500 5900 2500 6200
Wire Wire Line
	2750 5900 2750 6250
Wire Wire Line
	2500 3600 2400 3600
Wire Wire Line
	2500 3200 2500 3600
Wire Wire Line
	2000 3200 2500 3200
Wire Wire Line
	2000 2250 2000 3200
Wire Wire Line
	1300 3600 1200 3600
Wire Wire Line
	1200 3600 1200 3200
Wire Wire Line
	1200 3200 1750 3200
Wire Wire Line
	1750 3200 1750 2250
Wire Wire Line
	1650 2500 1750 2500
Connection ~ 1750 2500
Wire Wire Line
	1450 2250 1450 2350
Wire Wire Line
	1450 2350 1750 2350
Connection ~ 1750 2350
Wire Wire Line
	2300 2250 2300 2350
Wire Wire Line
	2300 2350 2000 2350
Connection ~ 2000 2350
Wire Wire Line
	2100 2500 2000 2500
Connection ~ 2000 2500
Wire Wire Line
	2700 1350 2750 1350
Wire Wire Line
	2750 1300 2750 2500
Wire Wire Line
	2750 2500 2500 2500
Wire Wire Line
	1050 1350 1000 1350
Wire Wire Line
	1000 1300 1000 2500
Wire Wire Line
	1000 2500 1250 2500
Wire Wire Line
	1600 750  1600 950 
Wire Wire Line
	900  750  2850 750 
Wire Wire Line
	1000 750  1000 800 
Connection ~ 1000 1350
Wire Wire Line
	900  750  900  4000
Wire Wire Line
	900  4000 1300 4000
Connection ~ 1000 750 
Wire Wire Line
	1050 1850 800  1850
Wire Wire Line
	800  1850 800  1900
Wire Wire Line
	1050 1600 650  1600
Wire Wire Line
	650  1500 650  1900
Wire Wire Line
	1850 4450 1850 4550
Wire Wire Line
	2950 4500 800  4500
Wire Wire Line
	800  4500 800  2400
Connection ~ 1850 4500
Wire Wire Line
	650  2400 650  2450
Wire Wire Line
	650  2450 800  2450
Connection ~ 800  2450
Connection ~ 2750 1350
Wire Wire Line
	2150 750  2150 950 
Connection ~ 1600 750 
Wire Wire Line
	2750 750  2750 800 
Connection ~ 2150 750 
Wire Wire Line
	2850 750  2850 4000
Wire Wire Line
	2850 4000 2400 4000
Connection ~ 2750 750 
Wire Wire Line
	2700 1850 2950 1850
Wire Wire Line
	2950 1850 2950 1900
Wire Wire Line
	2700 1600 3100 1600
Wire Wire Line
	3100 1500 3100 1900
Wire Wire Line
	2950 2400 2950 4500
Wire Wire Line
	3100 2400 3100 2450
Wire Wire Line
	3100 2450 2950 2450
Connection ~ 2950 2450
Connection ~ 1850 750 
Connection ~ 650  1600
Wire Wire Line
	850  5600 1050 5600
Wire Wire Line
	650  5700 900  5700
Wire Wire Line
	900  5700 900  5600
Connection ~ 900  5600
Wire Wire Line
	3500 5200 5250 5200
Wire Wire Line
	1850 4900 1950 4900
Wire Wire Line
	3550 6100 4200 6100
Wire Wire Line
	3650 2000 3750 2000
Wire Wire Line
	3650 2200 3750 2200
Connection ~ 3100 1600
Wire Wire Line
	1850 5100 1950 5100
Wire Wire Line
	1850 5200 1950 5200
Wire Wire Line
	1900 6500 1800 6500
Wire Wire Line
	900  6600 1000 6600
Wire Wire Line
	2600 5200 2700 5200
Wire Wire Line
	2700 5300 2600 5300
Wire Wire Line
	3650 1900 3750 1900
Wire Wire Line
	3650 2100 3750 2100
Wire Wire Line
	5350 1900 5450 1900
Wire Wire Line
	5350 2000 5450 2000
Wire Wire Line
	5350 2100 5450 2100
Wire Wire Line
	5350 2200 5450 2200
Wire Wire Line
	2750 6850 2800 6850
Wire Wire Line
	900  6700 1000 6700
Wire Wire Line
	1800 6700 1900 6700
Wire Wire Line
	950  5000 1050 5000
Wire Wire Line
	950  5100 1050 5100
Wire Wire Line
	2600 4900 2700 4900
Wire Wire Line
	8100 1900 8100 2100
Connection ~ 8100 2050
Wire Wire Line
	6150 4300 6250 4300
Wire Wire Line
	6150 4000 6250 4000
Wire Wire Line
	6800 3850 6800 4000
Connection ~ 6800 4000
Wire Wire Line
	7200 3850 7200 4300
Connection ~ 7200 4300
Wire Wire Line
	6150 2300 6250 2300
Wire Wire Line
	6150 2600 6250 2600
Wire Wire Line
	7200 2750 7200 2300
Connection ~ 7200 2300
Wire Wire Line
	6800 2750 6800 2600
Connection ~ 6800 2600
Wire Wire Line
	7650 3300 7750 3300
Connection ~ 5400 800 
Wire Wire Line
	5400 600  5400 800 
Wire Wire Line
	5400 600  5350 600 
Wire Wire Line
	2650 4050 2650 4000
Connection ~ 2650 4000
Wire Wire Line
	2650 4450 2650 4500
Connection ~ 2650 4500
Wire Wire Line
	1100 4050 1100 4000
Connection ~ 1100 4000
Wire Wire Line
	1100 4450 1100 4500
Connection ~ 1100 4500
Wire Wire Line
	2500 3050 2950 3050
Connection ~ 2950 3050
Wire Wire Line
	2100 3050 2000 3050
Connection ~ 2000 3050
Wire Wire Line
	1650 3050 1750 3050
Connection ~ 1750 3050
Wire Wire Line
	1250 3050 800  3050
Connection ~ 800  3050
Wire Wire Line
	6750 2300 7550 2300
Wire Wire Line
	6750 2600 7550 2600
Wire Wire Line
	6750 4000 7550 4000
Wire Wire Line
	6750 4300 7550 4300
Wire Wire Line
	2100 2800 2000 2800
Connection ~ 2000 2800
Wire Wire Line
	1650 2800 1750 2800
Connection ~ 1750 2800
Wire Wire Line
	1850 750  1850 650 
Wire Wire Line
	1850 650  1750 650 
Wire Wire Line
	10400 6150 10800 6150
Wire Wire Line
	10400 6350 10500 6350
Wire Wire Line
	10400 6550 10500 6550
Wire Wire Line
	10400 6650 10500 6650
Wire Wire Line
	10400 6850 10500 6850
Wire Wire Line
	9100 6850 9600 6850
Wire Wire Line
	9500 6750 9600 6750
Wire Wire Line
	9500 6550 9600 6550
Wire Wire Line
	9500 6650 9600 6650
Wire Wire Line
	9500 6450 9600 6450
Wire Wire Line
	9500 6350 9600 6350
Wire Wire Line
	9500 6250 9600 6250
Wire Wire Line
	9500 6150 9600 6150
Wire Wire Line
	10400 6250 10700 6250
Wire Wire Line
	10700 6450 10400 6450
Wire Wire Line
	10700 6750 10400 6750
Wire Wire Line
	6950 1500 6950 1400
Wire Wire Line
	7250 1050 7550 1050
Wire Wire Line
	8650 1050 8950 1050
Wire Wire Line
	9250 1500 9250 1400
Wire Wire Line
	9250 1450 9200 1450
Connection ~ 9250 1450
Wire Wire Line
	8900 1100 8900 1050
Connection ~ 8900 1050
Wire Wire Line
	7300 1100 7300 1050
Connection ~ 7300 1050
Wire Wire Line
	7000 1450 6950 1450
Connection ~ 6950 1450
Wire Wire Line
	6950 2000 6950 2050
Wire Wire Line
	6950 2050 9250 2050
Wire Wire Line
	9250 2050 9250 2000
Wire Wire Line
	8900 1800 8900 2050
Connection ~ 8900 2050
Wire Wire Line
	7300 1800 7300 2050
Connection ~ 7300 2050
Wire Wire Line
	4500 2500 5350 2500
Wire Wire Line
	3700 2700 3600 2700
Wire Wire Line
	3600 2700 3600 2800
Wire Wire Line
	3600 2800 3700 2800
Wire Wire Line
	3700 2900 3600 2900
Wire Wire Line
	3600 2900 3600 3050
Wire Wire Line
	3600 3050 4600 3050
Wire Wire Line
	4600 3050 4600 2600
Wire Wire Line
	4600 2600 4500 2600
Wire Wire Line
	4500 2900 4600 2900
Connection ~ 4600 2900
Wire Wire Line
	4500 2800 4700 2800
Wire Wire Line
	4700 2800 4700 3400
Wire Wire Line
	4600 3400 4900 3400
Wire Wire Line
	4600 3500 5300 3500
Wire Wire Line
	4800 3500 4800 2700
Wire Wire Line
	4800 2700 4500 2700
Wire Wire Line
	4900 3400 4900 3250
Connection ~ 4700 3400
Wire Wire Line
	5300 3500 5300 3250
Connection ~ 4800 3500
Wire Wire Line
	5100 2750 5100 2500
Connection ~ 5100 2500
Wire Wire Line
	3750 3950 3650 3950
Wire Wire Line
	3650 3950 3650 4050
Wire Wire Line
	3650 4050 3750 4050
Wire Wire Line
	3750 4150 3650 4150
Wire Wire Line
	3650 4150 3650 4300
Wire Wire Line
	3650 4300 4650 4300
Wire Wire Line
	4650 4300 4650 3850
Wire Wire Line
	4650 3850 4550 3850
Wire Wire Line
	4550 3750 5400 3750
Wire Wire Line
	4550 4150 4650 4150
Connection ~ 4650 4150
Wire Wire Line
	4650 4650 4950 4650
Wire Wire Line
	4750 4650 4750 4050
Wire Wire Line
	4750 4050 4550 4050
Wire Wire Line
	4650 4750 5350 4750
Wire Wire Line
	4850 4750 4850 3950
Wire Wire Line
	4850 3950 4550 3950
Wire Wire Line
	4950 4650 4950 4500
Connection ~ 4750 4650
Wire Wire Line
	5350 4750 5350 4500
Connection ~ 4850 4750
Wire Wire Line
	5150 4000 5150 3750
Connection ~ 5150 3750
Wire Wire Line
	950  4900 1050 4900
$EndSCHEMATC
