PCBNEW-LibModule-V1  10/20/2014 11:09:58 AM
# encoding utf-8
Units mm
$INDEX
0Ω_Jumper
Battery_Holder
SOD80
SOT23_6L_SMS
so-16(w)
sot23_PESD
$EndINDEX
$MODULE 0Ω_Jumper
Po 0 0 0 15 543ECF05 00000000 ~~
Li 0Ω_Jumper
Sc 0
AR 
Op 0 0 0
T0 0.01 1.71 1 1 0 0.15 N V 21 N "0Ω"
T1 0.15 -1.52 1 1 0 0.15 N V 21 N "JMP"
$PAD
Sh "2" R 0.635 1.143 0 0 0
Dr 0 0 0 O 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "3" R 0.635 1.143 0 0 0
Dr 0 0 0 O 0 0
At SMD N 00888000
Ne 0 ""
Po 1.54 0
$EndPAD
$PAD
Sh "1" R 0.635 1.143 0 0 0
Dr 0 0 0 O 0 0
At SMD N 00888000
Ne 0 ""
Po -1.54 0
$EndPAD
$EndMODULE 0Ω_Jumper
$MODULE Battery_Holder
Po 0 0 0 15 543ECA92 00000000 ~~
Li Battery_Holder
Sc 0
AR 
Op 0 0 0
T0 -0.2 1 1 1 0 0.15 N V 21 N "Battery Holder"
T1 0.3 -0.6 1 1 0 0.15 N V 21 N "VAL**"
DA 0.2 0.1 11.2 2.8 900 0.15 21
DA 0.2 0.1 -2.5 -10.9 900 0.15 21
DA 0.2 0.1 2.9 11.1 900 0.15 21
DA 0.2 0.1 -10.8 -2.5 900 0.15 21
DS 12.6 -2.5 11.2 -2.5 0.15 21
DS 12.6 -2.5 12.6 2.8 0.15 21
DS 12.6 2.8 11.2 2.8 0.15 21
DS -14.3 -2.5 -14.3 2.7 0.15 21
DS -14.3 2.7 -10.8 2.7 0.15 21
DS -14.3 -2.5 -10.8 -2.5 0.15 21
$PAD
Sh "2" R 3.15 4.78 0 0 0
Dr 0 0 0 O 0 0
At SMD N 00888000
Ne 0 ""
Po 14.6 0.1
$EndPAD
$PAD
Sh "1" R 3.15 4.78 0 0 0
Dr 0 0 0 O 0 0
At SMD N 00888000
Ne 0 ""
Po -16.3 0.1
$EndPAD
$EndMODULE Battery_Holder
$MODULE SOD80
Po 0 0 0 15 54416CD6 00000000 ~~
Li SOD80
Sc 0
AR 
Op 0 0 0
T0 1.5 0.15 1.5 1.5 0 0.15 N I 21 N "SOD80"
T1 1.75 2.65 1.5 1.5 0 0.15 N V 21 N "VAL**"
DS 0 -1.15 0 1.15 0.15 21
DS -0.7 0 -0.7 -1.15 0.15 21
DS -0.7 -1.15 3.9 -1.15 0.15 21
DS 3.9 -1.15 3.9 1.15 0.15 21
DS 3.9 1.15 -0.7 1.15 0.15 21
DS -0.7 1.15 -0.7 0 0.15 21
$PAD
Sh "K" R 0.9 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "A" R 0.9 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.2 0
$EndPAD
$EndMODULE SOD80
$MODULE SOT23_6L_SMS
Po 0 0 0 15 54450643 00000000 ~~
Li SOT23_6L_SMS
Sc 0
AR /543BF901
Op 0 0 0
T0 1.99898 0 0.762 0.762 900 0.0762 N V 21 N "D4"
T1 0.0635 0 0.50038 0.50038 0 0.0762 N V 21 N "SMS24C"
DS -0.508 0.762 -1.27 0.254 0.127 21
DS 1.27 0.762 -1.3335 0.762 0.127 21
DS -1.3335 0.762 -1.3335 -0.762 0.127 21
DS -1.3335 -0.762 1.27 -0.762 0.127 21
DS 1.27 -0.762 1.27 0.762 0.127 21
$PAD
Sh "6" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "U4_15Vin"
Po -0.9398 -1.27
$EndPAD
$PAD
Sh "5" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -1.27
$EndPAD
$PAD
Sh "4" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 5 "U4_15Vout"
Po 0.9398 -1.27
$EndPAD
$PAD
Sh "3" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "U3_15Vin"
Po 0.9398 1.27
$EndPAD
$PAD
Sh "2" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 0 1.27
$EndPAD
$PAD
Sh "1" R 0.6604 1.016 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "U3_15Vout"
Po -0.9398 1.27
$EndPAD
$SHAPE3D
Na "smd/SOT23_6.wrl"
Sc 0.11 0.11 0.11
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE SOT23_6L_SMS
$MODULE so-16(w)
Po 0 0 0 15 54445973 00000000 ~~
Li so-16(w)
Cd SO-16(w)
Sc 0
AR /538786D8
Op 0 0 0
At SMD
T0 -2.159 -0.254 0.7493 0.7493 0 0.14986 N V 21 N "U2"
T1 0 1.016 0.7493 0.7493 0 0.14986 N V 21 N "DS3231SN"
DS -4.9 -3.9 -4.9 3.8 0.15 21
DS 4.7 -3.9 4.7 3.7 0.15 21
DS 4.7 3.7 4.7 3.5 0.15 21
DS -2.005 -3.8812 -2.005 -4.9734 0.127 21
DS -0.735 -3.8812 -0.735 -4.9734 0.127 21
DS 0.535 -3.8812 0.535 -4.9734 0.127 21
DS -3.275 -3.8812 -3.275 -4.9734 0.127 21
DS -4.545 -4.9734 -4.545 -3.8812 0.127 21
DS 1.805 -4.9734 1.805 -3.8812 0.127 21
DS 3.075 -4.9734 3.075 -3.8812 0.127 21
DS 4.345 -4.9734 4.345 -3.8812 0.127 21
DS 4.345 3.7812 4.345 4.8734 0.127 21
DS 3.075 3.7812 3.075 4.8734 0.127 21
DS 1.805 3.7812 1.805 4.8734 0.127 21
DS -4.545 3.7812 -4.545 4.8734 0.127 21
DS -3.275 4.8734 -3.275 3.7812 0.127 21
DS 0.535 4.8734 0.535 3.7812 0.127 21
DS -0.735 4.8734 -0.735 3.7812 0.127 21
DS -2.005 4.8734 -2.005 3.7812 0.127 21
DC -4.0513 2.8628 -4.3307 3.2438 0.127 21
DS -4.926 3.7812 4.726 3.7812 0.127 21
DS 4.726 -3.8812 -4.926 -3.8812 0.127 21
$PAD
Sh "1" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 0 ""
Po -4.545 4.594
$EndPAD
$PAD
Sh "2" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 5 "VDD"
Po -3.275 4.594
$EndPAD
$PAD
Sh "3" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 0 ""
Po -2.005 4.594
$EndPAD
$PAD
Sh "4" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 0 ""
Po -0.735 4.594
$EndPAD
$PAD
Sh "5" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 0.535 4.594
$EndPAD
$PAD
Sh "6" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 1.805 4.594
$EndPAD
$PAD
Sh "7" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 3.075 4.594
$EndPAD
$PAD
Sh "8" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 4.345 4.594
$EndPAD
$PAD
Sh "9" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 4.345 -4.694
$EndPAD
$PAD
Sh "10" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 3.075 -4.694
$EndPAD
$PAD
Sh "11" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 1.805 -4.694
$EndPAD
$PAD
Sh "12" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po 0.535 -4.694
$EndPAD
$PAD
Sh "13" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 1 "GND"
Po -0.735 -4.694
$EndPAD
$PAD
Sh "14" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 4 "N-0000076"
Po -2.005 -4.694
$EndPAD
$PAD
Sh "15" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 3 "N-00000101"
Po -3.275 -4.694
$EndPAD
$PAD
Sh "16" R 0.635 1.27 0 0 0
Dr 0 0 0
At SMD N 00008000
Ne 2 "N-00000100"
Po -4.545 -4.694
$EndPAD
$SHAPE3D
Na "smd/smd_dil/so-16.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE so-16(w)
$MODULE sot23_PESD
Po 0 0 0 15 5440493E 00000000 ~~
Li sot23_PESD
Cd SOT23
Sc 0
AR /543EABC0
Op 0 0 0
At SMD
T0 0 0 0.50038 0.50038 0 0.09906 N V 21 N "D7"
T1 0 0.09906 0.50038 0.50038 0 0.09906 N I 21 N "PESD"
DS 0.9525 0.6985 0.9525 1.3589 0.127 21
DS -0.9525 0.6985 -0.9525 1.3589 0.127 21
DS 0 -0.6985 0 -1.3589 0.127 21
DS -1.4986 -0.6985 1.4986 -0.6985 0.127 21
DS 1.4986 -0.6985 1.4986 0.6985 0.127 21
DS 1.4986 0.6985 -1.4986 0.6985 0.127 21
DS -1.4986 0.6985 -1.4986 -0.6985 0.127 21
$PAD
Sh "1" R 0.59944 1.00076 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "N-0000041"
Po -0.9525 1.05664
$EndPAD
$PAD
Sh "3" R 0.59944 1.00076 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "N-0000040"
Po 0 -1.05664
$EndPAD
$PAD
Sh "2" R 0.59944 1.00076 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "GND"
Po 0.9525 1.05664
$EndPAD
$SHAPE3D
Na "smd/smd_transistors/sot23.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE sot23_PESD
$EndLIBRARY
